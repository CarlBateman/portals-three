# Portals in Three.js

## Live page(s)
### Current version
<https://carlbateman.gitlab.io/portals-three/>


## To do
1. Add environment
   1. Add floors
   1. Add shapes
   1. Find models
   1. Add models
1. Track/mirror player relative to portal

## Done
1. Tidy code
